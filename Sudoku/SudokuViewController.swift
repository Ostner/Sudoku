//
//  SudokuViewController.swift
//  Sudoku
//
//  Created by Tobias Ostner on 6/21/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class SudokuViewController: UIViewController, InputViewControllerDelegate {

    var difficulty: Sudoku.difficulty?

    var sudoku: Sudoku!

    @IBOutlet weak var sudokuView: SudokuView!

    override func viewDidLoad() {
        sudoku = generateSudoku()
        for index in sudoku.presetCells {
            sudokuView.setNumber(
                number: "\(sudoku[index])",
                at: index,
                isPreset: true)
        }
        for index in 0..<sudoku.cellCount where !sudoku.presetCells.contains(index) {
            let tap = UITapGestureRecognizer(target: self, action: #selector(cellTapped))
            sudokuView.cells[index].addGestureRecognizer(tap)
        }
    }

    @IBAction func newSudoku() {
        dismiss(animated: true)
    }

    @objc func cellTapped(sender: UITapGestureRecognizer) {
        let inputVC = storyboard!.instantiateViewController(withIdentifier: "InputViewController") as! InputViewController
        inputVC.modalPresentationStyle = .custom
        inputVC.cellIndex = sudokuView.cells.index(of: sender.view as! UILabel)
        inputVC.delegate = self
        present(inputVC, animated: true)
    }

    // MARK: InputViewControllerDelegate

    func setNumber(_ number: String, at index: Int) {
        sudokuView.setNumber(number: number, at: index)
        dismiss(animated: true)
    }

    // MARK: Private

    private func generateSudoku() -> Sudoku {
        let fillCount: Int
        if let difficulty = difficulty {
            switch difficulty {
            case .light: fillCount = 50
            case .easy: fillCount = 35
            case .difficult: fillCount = 27
            case .impossible: fillCount = 20
            }
        } else {
            fillCount = 50
        }
        return Sudoku.generateSudoku(fillCount: fillCount)
    }

}
