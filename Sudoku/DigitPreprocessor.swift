//
//  DigitPreprocessor.swift
//  DigitClassifier
//
//  Created by Tobias Ostner on 6/29/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class DigitPreprocessor {

    let originalImage: UIImage
    var input: [Float32] = []

    let digitSize = CGSize(width: 20, height: 20)
    let imageSize = CGSize(width: 28, height: 28)

    init(image: UIImage) {
        originalImage = image
    }

    var inputImage: UIImage {
        let pixel = input.map { UInt8($0 * 255)}
        print(pixel)
        return draw(pixelData: pixel)
    }

    func run() {
        input = encode(image: center(image: scale(image: originalImage)))
    }

    //Todo: The scaling doesn't messes up the aspect ratio (ex: the one becomes very thick).
    private func scale(image: UIImage) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: digitSize)
        return renderer.image { _ in
            image.draw(in: CGRect(origin: .zero, size: digitSize))
        }
    }

    private func center(image: UIImage) -> UIImage {
        let renderer = UIGraphicsImageRenderer(size: imageSize)
        return renderer.image { _ in
            image.draw(at: CGPoint(x: 4, y: 4))
        }
    }

    private func encode(image: UIImage) -> [Float32] {
        return image.alphaValues!.map { Float32($0) / 255 }
    }

    private func draw(pixelData: [UInt8]) -> UIImage {
        let provider = CGDataProvider(
            dataInfo: nil,
            data: pixelData,
            size: Int(imageSize.width * imageSize.height),
            releaseData: {_, _, _ in })!
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let cgImage = CGImage(
            width: 28,
            height: 28,
            bitsPerComponent: 8,
            bitsPerPixel: 8,
            bytesPerRow: 28,
            space: colorSpace,
            bitmapInfo: .byteOrderMask,
            provider: provider,
            decode: nil,
            shouldInterpolate: false,
            intent: .defaultIntent)!
        return UIImage(cgImage: cgImage)

    }
}
