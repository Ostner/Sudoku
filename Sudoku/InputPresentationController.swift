//
//  InputPresentationController.swift
//  Sudoku
//
//  Created by Tobias Ostner on 7/14/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class InputPresentationController: UIPresentationController {

    let width: CGFloat = 400
    let height: CGFloat = 400

    let chrome = UIView()

    override init(presentedViewController presented: UIViewController,
                  presenting: UIViewController?)
    {
        super.init(presentedViewController: presented, presenting: presenting)
        chrome.backgroundColor = UIColor(white: 0, alpha: 0.6)
        let tap = UITapGestureRecognizer(target: self, action: #selector(chromeTapped))
        chrome.addGestureRecognizer(tap)
    }

    @objc func chromeTapped() {
        presentingViewController.dismiss(animated: true)
    }

    override var frameOfPresentedViewInContainerView: CGRect {
        let x = (containerView!.bounds.width - width) / 2
        let y = (containerView!.bounds.height - height) / 2
        return CGRect(x: x, y: y, width: width, height: height)
    }

    override func presentationTransitionWillBegin() {
        chrome.frame = containerView!.bounds
        containerView!.insertSubview(chrome, at: 0)
    }
}
