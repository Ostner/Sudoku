//
//  StartViewController.swift
//  Sudoku
//
//  Created by Tobias Ostner on 7/10/17.
//  Copyright © 2017 Tobias Ostner. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {
    
    @IBOutlet var difficulties: [DifficultyButton]!
    @IBOutlet var titleLabel: UILabel!

    enum difficulty {
        case light, easy, difficult, impossible
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        //TODO: Setting the color isn't possible in IB yet
        view.backgroundColor = UIColor(named: "Sea")
        titleLabel.textColor = UIColor(named: "BurntOrange")
        difficulties.forEach {
            $0.backgroundColor = UIColor(named: "Sandstone")
            $0.textColor = UIColor(named: "BurntOrange")
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let sender = sender as? UITapGestureRecognizer,
              let btn = sender.view as? DifficultyButton,
              let sudokuVC = segue.destination as? SudokuViewController
        else { fatalError("Segue is not defined") }
        sudokuVC.difficulty = Sudoku.difficulty(rawValue: btn.text.lowercased())
    }

}
