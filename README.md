# Sudoku Prototype 1

* Play Sudokus with four different difficulty levels.
* Set numbers by tapping the cell and drawing on a canvas.

![Demo](Resources/SudokuP1.gif)
